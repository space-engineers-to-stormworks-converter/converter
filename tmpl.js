"use strict";

window.TMPL = (() => {

  function getTmpl(name) {
    if($('[tmpl-name="'+name+'"]').length){
      let str = $('[tmpl-name="'+name+'"]').html()
      str = str.replace('<'+'!-'+'-', '')
      str = str.replace('--'+'>', '')
      return str;
    } else {
      console.error('cant find template with name ', name);
      return '';
    }
  }

  return function(template_name, data){
    let template = Handlebars.compile( getTmpl(template_name) );
    return template(data);
  }
})()