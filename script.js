/* Model Analyzer */

((global, $)=>{
  "use strict";

  let result_container 
  let fileinput


  let MAPPINGS = {}

  const TYPE_MICROPROCESSOR = 'microprocessor'

  const CATEGORY_LOADING_FILE = 0
  const CATEGORY_PARSING_XML = 1
  const CATEGORY_CONVERTING = 2
  const CATEGORY_BUILD_XML = 3
  const CATEGORY_BUILD_STATS = 4
  const CATEGORY_LABELS = ['Loading File', 'Parsing XML', 'Converting', 'Building XML', 'Gathering Statistics']


  let missingMappings = {}

    $.getJSON('/mappings.json', (data)=>{
      MAPPINGS = data
      log('\n\nMAPPINGS loaded!\n\n')
    })
  

    $(window).on('load', ()=>{
	    fileinput = $('#file')
	    result_container = $('#result')

	    fileinput.on('change', (evt)=>{
	        checkFileInformation()
	    })
	    checkFileInformation()
    })

	function checkFileInformation(){

	    let file = fileinput.get(0).files[0]
	    if(file){
	        console.log(file)
	        $('#selection #file-information').html(''+file.name + '   ' + parseInt(file.size / 10000.0) / 100 + ' MB')
	        $('#selection #analyze-button').removeAttr('disabled')
	    } else {
	        $('#selection #file-information').html('-')
	        $('#selection #analyze-button').attr('disabled', 'true')
	    }
	}

	function convertBlob(){
		missingMappings = {}
		if(Object.keys(MAPPINGS).length === 0){
			alert('could not load mappings (local). Go to https://space-works-converter.flaffipony.rocks')
			return
		}

	    if(! fileinput){
	        return console.error('Fileinput is not defined!')
	    }
	    let file = fileinput.get(0).files[0]
	    if(!file){
	        return handleFileError('no file choosen')
	    }
	    startAnalyzingProgress()
	    let fr = new FileReader()
	    fr.onload = (data)=>{
	        handleFileProgress(true)
	        processXML(fr.result).then(()=>{
	        	console.log('missing mappings:', Object.keys(missingMappings).join(','))
		        stopAnalyzingProgress()
		        $('#selection').fadeOut(200, ()=>{
		            $('#result-container').fadeIn(200)
		        })
		    })
	    }
	    fr.onerror = (evt)=>{
	      handleFileError(evt)
	    }

	    fr.onprogress = (evt)=>{
	      handleFileProgress(evt)
	    }
	    log('reading blob (' + file.size / 1000 + ' kB)')
	    fr.readAsText(file)
	}

	function processXML(xml){
	  	return new Promise((fulfill, reject)=>{
		    const options = {
		        attributeNamePrefix : "",
		        ignoreAttributes : false,
		        ignoreNameSpace : false,
		        allowBooleanAttributes : true,
		        parseNodeValue : true,
		        parseAttributeValue : false,
		        trimValues: true,
		        parseTrueNumberOnly: false,
		        attrValueProcessor: a => he.decode(a, {isAttributeValue: true}),//default is a=>a
		        tagValueProcessor : a => he.decode(a) //default is a=>a
		    }



		    handleAnalyzingProgress(CATEGORY_PARSING_XML, 0)
		    let obj = XMLParser.parse(xml, options);
		    handleAnalyzingProgress(CATEGORY_PARSING_XML, 1)

		    console.log(obj)


		    let compat = checkCompatibility(obj)
		      
		    if(compat !== true){
		        handleFileError('not compatible: ' + compat)
		        fulfill()
		        return
		    }
		    

		    convert(obj).then((converted)=>{
		    	buildXML(converted).then((xml)=>{

		    		buildStats(obj, converted).then((html)=>{
		    			let filename
		    			if(obj.Definitions.ShipBlueprints.ShipBlueprint instanceof Array){
		    				filename = obj.Definitions.ShipBlueprints.ShipBlueprint[0].Id.Subtype
		    			} else if (obj.Definitions.ShipBlueprints.ShipBlueprint.CubeGrids instanceof Object){
		    				filename = obj.Definitions.ShipBlueprints.ShipBlueprint.Id.Subtype
		    			} else {
			    			filename = obj.Definitions.ShipBlueprints.ShipBlueprint.DisplayName			    			
			    		}
		    			if(typeof filename !== 'string' || filename.length === 0){
		    				filename = 'converted vehicle'
		    			}
		    			filename = filename.replace(/\W/g, '').trim() + '.xml'
		    			downloadXML(xml, filename)
			        	result_container.html('')
				    	result_container.append(html)
				    	fulfill()
		    		})
		    	})

		    })
		})
	}

	function checkCompatibility(obj){
		if(!obj.Definitions || !obj.Definitions.ShipBlueprints || ! obj.Definitions.ShipBlueprints.ShipBlueprint || ! obj.Definitions.ShipBlueprints.ShipBlueprint.CubeGrids || ! obj.Definitions.ShipBlueprints.ShipBlueprint.CubeGrids instanceof Array){
			return 'xml strucutre does not match excepted structure.'
		}
		if (! obj.Definitions.ShipBlueprints.ShipBlueprint["xsi:type"] === "MyObjectBuilder_ShipBlueprintDefinition"){
			return 'xsi:type does not match: "' + obj.Definitions.ShipBlueprints.ShipBlueprint["xsi:type"] + '"'
		}

	    return true
	}

  function convert(obj){
  	return new Promise((fulfill, reject)=>{

		handleAnalyzingProgress(CATEGORY_CONVERTING, 0)

		let converted = {
			vehicle: {
				"authors@author": [{
					steam_id: obj.Definitions.ShipBlueprints.ShipBlueprint.OwnerSteamId,
					username: obj.Definitions.ShipBlueprints.ShipBlueprint.DisplayName + " - Converted with space-works-converter"
				}],
				"bodies@body": [],
				bodies_id: "366",
				data_version: "2",
				editor_placement_offset: {x:"0", y:"0", z:"0"},
				is_advanced: "true",
				is_static: "false",
				"logic_node_links@logic_node_link": []
			}
		}

		if(typeof obj.Definitions.ShipBlueprints.ShipBlueprint.CubeGrids.CubeGrid === 'object' && obj.Definitions.ShipBlueprints.ShipBlueprint.CubeGrids.CubeGrid instanceof Array === false){
			let tmp = [obj.Definitions.ShipBlueprints.ShipBlueprint.CubeGrids.CubeGrid]
			obj.Definitions.ShipBlueprints.ShipBlueprint.CubeGrids.CubeGrid = tmp
		}

		let gridsDone = 0
		for(let cg of obj.Definitions.ShipBlueprints.ShipBlueprint.CubeGrids.CubeGrid){


			let offsets = cg.PositionAndOrientation.Position
			//TODO translate offsets into initial_local_transform and local_transform

			let convertedBody = {
				"components@c": [],
				unique_id: gridsDone+1,
				initial_local_transform: {
					"00": "1",
					"01": "0",
					"02": "0",
					"03": "0",
					"10": "0",
					"11": "1",
					"12": "0",
					"13": "0",
					"20": "0",
					"21": "0",
					"22": "1",
					"23": "0",
					"30": "0",
					"31": "0",
					"32": "0",
					"33": "1"
				},
				local_transform: {
					"00": "1",
					"01": "0",
					"02": "0",
					"03": "0",
					"10": "0",
					"11": "1",
					"12": "0",
					"13": "0",
					"20": "0",
					"21": "0",
					"22": "1",
					"23": "0",
					"30": "0",
					"31": "0",
					"32": "0",
					"33": "1"
				}
			}

			if(typeof cg.CubeBlocks.MyObjectBuilder_CubeBlock === 'object' && cg.CubeBlocks.MyObjectBuilder_CubeBlock instanceof Array === false){
				let tmp = [cg.CubeBlocks.MyObjectBuilder_CubeBlock]
				cg.CubeBlocks.MyObjectBuilder_CubeBlock = tmp
			}

			for(let cb of cg.CubeBlocks.MyObjectBuilder_CubeBlock){
				let type = cb.SubtypeName
				let color = cb.ColorMaskHSV
				let min = cb.Min
				let orientation = cb.BlockOrientation

				if(!min){
					console.warn('min is invalid, skipping block:', type, min)
				} else {
					if(!color){
						console.warn('color is invalid, using white.', type, color)
						color = {x:100,y:0,z:0}
					}
					let h = limit(0, 360, parseFloat(color.x)*360)
					let s = limit(0, 1, (parseFloat(color.y)+0.68)*1.1363636)
					let v = limit(0, 1, parseFloat(color.z)+0.45)
					let rgbColor = rgbFromHSV(h, s, v)

					if(!rgbColor || isNaN(rgbColor.r) || isNaN(rgbColor.g) || isNaN(rgbColor.b)){
						console.warn('calculated hsv color is invalid, using white.', color)
						rgbColor = {r:150, g:150, b:150}
					}

					if(! typeSupported(type)){
						console.log('skipping unsupported block type')
					} else {
						convertedBody["components@c"].push({
							d: map(type),
							o: {
								r: orientationToRotation(orientation),
								bc: fullRgbToHex(rgbColor.r, rgbColor.g, rgbColor.b),
								ac: fullRgbToHex(rgbColor.r, rgbColor.g, rgbColor.b),
								sc: "6",
								vp: {
									x: min.z,
									y: min.y,
									z: -min.x
								},
							},
							t: "0"
						})
					}
				}
			}


			converted.vehicle["bodies@body"].push(convertedBody)

			gridsDone++
			handleAnalyzingProgress(CATEGORY_CONVERTING, gridsDone/obj.Definitions.ShipBlueprints.ShipBlueprint.CubeGrids.length)
		}


		handleAnalyzingProgress(CATEGORY_CONVERTING, 1)
	    fulfill(converted)
  	})
  }

  function buildXML(converted){
  	return new Promise((fulfill, reject)=>{

		handleAnalyzingProgress(CATEGORY_BUILD_XML, 0)

		console.log('building xml from', converted)

		let xmlDoc = document.implementation.createDocument(null, "vehicle");

		let serializer2 = new XMLSerializer();
		let xmlString2 = serializer2.serializeToString(xmlDoc);

		processChildren(xmlDoc.getElementsByTagName("vehicle")[0], converted.vehicle)

		function processChildren(rootNode, obj){
			for(let k of Object.keys(obj)){
				let v = obj[k]
				if(v instanceof Array){
					try {
						let node = xmlDoc.createElement(k.split("@")[0]);
						rootNode.appendChild(node)
						for(let e of v){
							let node2 = xmlDoc.createElement(k.split("@")[1]);
							node.appendChild(node2)
							processChildren(node2, e)
						}
					} catch (ex){
						console.error('exception while building XML@Array: @k', k, '@v', v, 'exception:', ex)
					}
				} else if(typeof v === 'object' && v !== null && v !== undefined){
					try {
						let node = xmlDoc.createElement(k.split("@")[0]);
						rootNode.appendChild(node)
						processChildren(node, v)
					} catch (ex){
						console.error('exception while building XML@Object: @k', k, '@v', v, 'exception:', ex)
					}
				} else {
					try {
						if(!isNaN( parseInt(k.substring(0,1)) )){
							k = '_' + k //because attributes starting with numbers is no valid xml
						}
						rootNode.setAttribute(k, (v === undefined || v === null) ? '' : v)							
					} catch (ex){
						console.error('exception while building XML@else: @k', k, '@v', v, 'exception:', ex)
					}
				}
			}
		}

		let serializer = new XMLSerializer();
		let xmlString = serializer.serializeToString(xmlDoc);

		xmlString = xmlString.replace(/\s_([\d]+)=/g, ' $1=')

		xmlString = '<?xml version="1.0" encoding="UTF-8"?>\n' + xmlString

		console.log('buildXML', xmlDoc, xmlString)

		handleAnalyzingProgress(CATEGORY_BUILD_XML, 1)
	    fulfill(xmlString)
  	})
  }

  function buildStats(obj, converted){
  	return new Promise((fulfill, reject)=>{

		handleAnalyzingProgress(CATEGORY_BUILD_STATS, 0)

		let name = obj

		handleAnalyzingProgress(CATEGORY_BUILD_STATS, 1)
	    fulfill('')
  	})
  }

  function downloadXML(data, filename){
  	//serve as a downloadable file

    let blob = new Blob([data], {type: 'text/csv'});
    if(window.navigator.msSaveOrOpenBlob) {
        window.navigator.msSaveBlob(blob, filename);
    }
    else{
        let elem = window.document.createElement('a');
        elem.href = window.URL.createObjectURL(blob);
        elem.download = filename;        
        document.body.appendChild(elem);
        elem.click();        
        document.body.removeChild(elem);
    }
  }

  /* cuts of the tho hex chars for opacity 000000>FF< */
  function cleanColor(hex){
    if(typeof hex !== 'string'){
      return ''
    }
    if(hex.length > 8){
      return hex.toLowerCase()
    }
    if(hex.length === 8){
      return hex.substring(0,6).toLowerCase()
    }
    if(hex.length === 3){
      // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
      let shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
      return hex.replace(shorthandRegex, function(m, r, g, b) {
          return r + r + g + g + b + b;
      }).toLowerCase()
    }

    return hex.toLowerCase()
  }

  function hexToRgb(hex) {
    if(typeof hex !== 'string'){
      return ''
    }
    

    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    /*return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
    } : null;*/
    return result ? ( parseInt(result[1], 16) + ', ' + parseInt(result[2], 16) + ', ' + parseInt(result[3], 16) ) : ''
  }

  function fullRgbToHex(r,g,b,a){
    if(typeof r !== 'number' || typeof g !== 'number' || typeof b !== 'number'){
    	console.error('@FullRgbToHex: r,g or b was not a number!')
    	return false
    }
    try {
      let rr = parseInt(r)
      let gg = parseInt(g)
      let bb = parseInt(b)
      let aa = parseInt(typeof a === 'number' ? a : 255)
      return rgbToHex(rr) + rgbToHex(gg) + rgbToHex(bb) + rgbToHex(aa)
    } catch (ex){
    	console.error('@FullRgbToHex', ex)
      return false
    }
  }

  function rgbToHex(number){
    var hex = Number(number).toString(16);
    if (hex.length < 2) {
         hex = "0" + hex;
    }
    return hex;
  }

  function rgbFromHSV(h,s,v) {
	  /**
	   * I: An array of three elements hue (h) ∈ [0, 360], and saturation (s) and value (v) which are ∈ [0, 1]
	   * O: An array of red (r), green (g), blue (b), all ∈ [0, 255]
	   * Derived from https://en.wikipedia.org/wiki/HSL_and_HSV
	   * This stackexchange was the clearest derivation I found to reimplement https://cs.stackexchange.com/questions/64549/convert-hsv-to-rgb-colors
	   */

	  let hprime = h / 60;
	  const c = v * s;
	  const x = c * (1 - Math.abs(hprime % 2 - 1)); 
	  const m = v - c;
	  let r, g, b;
	  if (!hprime) {r = 0; g = 0; b = 0; }
	  if (hprime >= 0 && hprime < 1) { r = c; g = x; b = 0}
	  if (hprime >= 1 && hprime < 2) { r = x; g = c; b = 0}
	  if (hprime >= 2 && hprime < 3) { r = 0; g = c; b = x}
	  if (hprime >= 3 && hprime < 4) { r = 0; g = x; b = c}
	  if (hprime >= 4 && hprime < 5) { r = x; g = 0; b = c}
	  if (hprime >= 5 && hprime < 6) { r = c; g = 0; b = x}
	  
	  r = Math.round( (r + m)* 255);
	  g = Math.round( (g + m)* 255);
	  b = Math.round( (b + m)* 255);

	  return {r:r, g:g, b:b}
  }


  /* Mapping */

  function map(key){
    if(MAPPINGS[key]){  	
     	return MAPPINGS[key]
    } else {
    	missingMappings[key] = true
      	return undefined
    }
  }

  function typeSupported(type){
  	return map(type) !== undefined
  }


  /* Helpers */  

	function limit(from, to, value){
		return Math.max(from, Math.min(to, value))
	}



	function orientationToRotation(orientation){
		if(orientation && orientation.Forward && orientation.Up){
			switch(orientation.Forward){
				case "Down": {
					switch(orientation.Up){
						case "Right": return "1,0,0,0,1,0,0,0,1"
						case "Left": return "-1,0,0,0,1,0,0,0,-1"
						case "Forward": return "0,0,1,0,1,0,-1,0,0"
					}
				}
				case "Forward": {
					switch(orientation.Up){
						case "Right": return "0,1,0,-1,0,0,0,0,1"
						case "Left": return "0,-1,0,-1,0,0,0,0,-1"
					}
				}
				case "Backward": {
					switch(orientation.Up){
						case "Right": return "0,-1,0,1,0,0,0,0,1"//"0,0,1,1,0,0,0,1,0"
						case "Left": return "0,1,0,1,0,0,0,0,-1"//"1,0,0,0,-1,0,0,0,-1"
						case "Down": return "0,0,1,1,0,0,0,1,0"//"0,-1,0,1,0,0,0,0,1"
					}
				}
				case "Up": {
					switch(orientation.Up){
						case "Right": return "1,0,0,0,0,-1,0,1,0"//"1,0,0,0,-1,0,0,0,-1"
						case "Left": return "1,0,0,0,-1,0,0,0,-1"
						case "Backward": return "0,0,-1,-1,0,0,0,1,0"
					}
				}
			}
		}

		return "0,0,-1,0,1,0,1,0,0"
	}



  function startAnalyzingProgress(){
    $('#analyze-progress').show()
    $('#analyze-progress .category').html(CATEGORY_LABELS[0])
    $('#analyze-progress .progress').html('0 %')
    $('#analyze-progress .step').html('1/' + CATEGORY_LABELS.length)
  }

  function stopAnalyzingProgress(){
    $('#analyze-progress').hide()
    $('#analyze-progress .category').html('')
    $('#analyze-progress .progress').html('')
    $('#analyze-progress .step').html('')
  }

  function handleAnalyzingProgress(category, progress){
    //console.log('handleAnalyzingProgress', category, progress)
    $('#analyze-progress .category').html(CATEGORY_LABELS[category])
    $('#analyze-progress .progress').html( parseInt(progress * 100.0) + ' %' )
    $('#analyze-progress .step').html(category+1 + '/' + CATEGORY_LABELS.length)
  }


  function handleFileError(evt){
    console.error(evt)
    $(result_container).html(evt.toString())
  }

  function handleFileProgress(evt){
    if(evt === true){
      handleAnalyzingProgress(CATEGORY_LOADING_FILE, 1)
      $(result_container).css('cssText', '') 
      return
    }
    handleAnalyzingProgress(CATEGORY_LOADING_FILE, evt.loaded / evt.total)
    let percent = parseInt(evt.loaded / evt.total * 10000) / 100.0
    $(result_container).css('background', 'linear-gradient(to right, blue 0%, blue ' + percent + '%, white ' + (percent+0.01) + '%, white 100%)')
  }

  function log(msg){
    let args = []
    for(let a of arguments){
      args.push(a)
    }
    console.log.apply(console, ['ModelAnalyzer:'].concat(args))
  }




  function checkSizes(){
    let min = Math.min($(window).height(), $(window).width()) / 2

    $('#selection').css({width: min+'px', height: min+'px'})
  }

  $(window).on('load', checkSizes)
  $(window).on('resize', checkSizes)
  

  global.ModelConverter = {
    convertBlob: convertBlob
  }

})(window, jQuery)
